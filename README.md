# InCore Semiconductor Docker Images

This repository contains Dockerfiles for InCore Semiconductor cores.  

Currently, it only contains a Dockerfile for the Chromite Core, which also works as a development environment for the Shakti C class processor.  

This repository and its `.gitlab-ci.yml` are designed so that:

* Every image will be built at every commit.  
  * It may be possible to build only images in folders with Dockerfiles that have changed on that commmit, but we're not certain of that. 
  * Images will be built in paralell, so that the wait is not obscenely long
* Developers new to the ecosystem can pull these images and have a certified, fully-working development environment in minutes instead of hours to days
* Image builds trigger core builds for testing purposes using `Pipeline Subscriptions`
* Repositories containing cores can use exactly the same development environment that developers use

Overall, this is an attempt at improving the accessiblity and developer experience of Open Source CPU cores.  

These images should make it easier for new developers to break into the space, and comments in the Dockerfiles, along with docs on the build process should make it easier to understand the steps that a core takes along its journey to running in simulation, on an FPGA, and finally existing as an ASIC.  